import numpy as np
import matplotlib.pyplot as plt
import math

def Rz(theta):
  return np.array([[np.cos(theta), -np.sin(theta), 0],
                   [np.sin(theta),  np.cos(theta), 0],
                   [0            ,  0            , 1]])

def create_matrix(translation, rotation, scale=1):
  return np.array([[rotation[0][0], rotation[1][0], rotation[2][0], translation[0]],
                   [rotation[0][1], rotation[1][1], rotation[2][1], translation[1]],
                   [rotation[0][2], rotation[1][2], rotation[2][2], translation[2]],
                   [0             , 0             , 0             , scale]])

def format_laser_data(range_data, scan_range, step_size, max_sensor_range=5):
  laser_data = []
  range_data = np.asarray(range_data)
  pts = math.floor(scan_range / step_size)
  angle = -scan_range * 0.5
  for i in range(pts):
    dist = range_data[i]        
    if dist <= 0:
      dist = max_sensor_range
    laser_data.append([angle, dist])
    angle = angle + step_size
  return np.array(laser_data)

def discretize_pos(x, y, config):
  min_x, max_x = config["map-x-limits"]
  min_y, max_y = config["map-y-limits"]
  grid_size_x, grid_size_y = config["grid-size"]

  clipped_x = np.clip(x, min_x, max_x)
  clipped_y = np.clip(y, min_y, max_y)
  new_x = int(np.floor((clipped_x - min_x) / (max_x - min_x) * grid_size_x))
  new_y = int(np.floor((clipped_y - min_y) / (max_y - min_y) * grid_size_y))
  return min(new_x, grid_size_x - 1), min(int(new_y), grid_size_y - 1)