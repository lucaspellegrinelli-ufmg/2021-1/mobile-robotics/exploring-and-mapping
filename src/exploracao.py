import numpy as np
import matplotlib.pyplot as plt
from skimage.draw import line
import sim
import time

from utils import *

configs = {
  "grid-size": (50, 50),
  "map-x-limits": (-5, 5),
  "map-y-limits": (-5, 5),
  "runtime-limit": 40,
  "sensor-max-dist": 5,
  "noise-std": 0
}

grid = np.ones(configs["grid-size"]) / 2

l_occ = np.log(0.65 / 0.35)
l_free = np.log(0.35 / 0.65)
l_0 = 0

def update_map(readings, r=0.1):
  global grid
  for reading in readings:
    grid_line = reading[0]
    dist = reading[1]
    p_line = np.zeros(len(grid_line))
    for i in range(len(grid_line)):
      line_dist = (dist / len(grid_line)) * (i + 1)
      if line_dist < (dist - r / 2):
        p_line[i] = l_free
      elif line_dist >= (dist - r / 2) and line_dist <= (dist + r / 2):
        p_line[i] = l_occ
      else:
        p_line[i] = 0.5

    for pos, val in zip(grid_line, p_line):
      grid[pos] = grid[pos] + val - l_0

print ("Program started")
sim.simxFinish(-1)
clientID = sim.simxStart("127.0.0.1", 19999, True, True, 5000, 5)

if clientID != -1:
  print ("Connected to remote API server")

  # Handle para o ROBÔ    
  robotname = "Pioneer_p3dx"
  returnCode, robotHandle = sim.simxGetObjectHandle(clientID, robotname, sim.simx_opmode_oneshot_wait)     
  
  # Handle para as juntas das RODAS
  returnCode, l_wheel = sim.simxGetObjectHandle(clientID, robotname + "_leftMotor", sim.simx_opmode_oneshot_wait)
  returnCode, r_wheel = sim.simxGetObjectHandle(clientID, robotname + "_rightMotor", sim.simx_opmode_oneshot_wait)    
  
  # Handle para os dados do LASER
  laser_data_name = "hokuyo_range_data"
  
  # Geralmente a primeira leitura é inválida (atenção ao Operation Mode)
  # Em loop até garantir que as leituras serão válidas
  returnCode = 1
  while returnCode != 0:
    returnCode, range_data = sim.simxGetStringSignal(clientID, laser_data_name, sim.simx_opmode_streaming + 10)
    returnCode, floor = sim.simxGetObjectHandle(clientID, "ResizableFloor_5_25", sim.simx_opmode_oneshot_wait)
  
  # Prosseguindo com as leituras
  returnCode, string_range_data = sim.simxGetStringSignal(clientID, laser_data_name, sim.simx_opmode_buffer)
  raw_range_data = sim.simxUnpackFloats(string_range_data)
  
  scan_range = 180 * np.pi / 180
  step_size = 2 * np.pi / 1024
  laser_data = format_laser_data(raw_range_data, scan_range, step_size)       
  
  # Dados do Pioneer
  L = 0.381   # Metros
  r = 0.0975  # Metros

  t = 0
  startTime = time.time()
  lastTime = startTime
  iteration = -1

  while t < configs["runtime-limit"]:
    iteration += 1
    now = time.time()
    dt = now - lastTime

    # Fazendo leitura do laser   
    returnCode = 1
    while returnCode != 0:
      returnCode, string_range_data = sim.simxGetStringSignal(clientID, laser_data_name, sim.simx_opmode_buffer)
      raw_range_data = sim.simxUnpackFloats(string_range_data)
      laser_data = format_laser_data(raw_range_data, scan_range, step_size)

    returnCode = 1
    while returnCode != 0:
      # Encontra a posição do robô em relação ao mundo
      returnCode, robot_t = sim.simxGetObjectPosition(clientID, robotHandle, -1, sim.simx_opmode_streaming)

      # Encontra a rotação relativa do chão em relação ao robô
      returnCode, floor_r = sim.simxGetObjectOrientation(clientID, floor, robotHandle, sim.simx_opmode_streaming)

    # Cria a matriz de transformação
    floor_m = create_matrix(robot_t, Rz(floor_r[2]))
    
    # Velocidade básica (linear, angular)
    v = 0
    w = np.deg2rad(0)      

    # Lógica de movimentação. Caso a frente esteja vazia, ande para a frente, caso contrário vire
    # para um lado vazio
    frente = int(len(laser_data) / 2)
    lado_direito = int(len(laser_data) * 1 / 4)
    lado_esquerdo = int(len(laser_data) * 3 / 4)
    
    if laser_data[frente, 1] > 2:
      v = 0.5
      w = 0
    elif laser_data[lado_direito, 1] > 2 or laser_data[lado_direito, 1] > laser_data[lado_esquerdo, 1]:
      v = 0
      w = np.deg2rad(-30)
    elif laser_data[lado_esquerdo, 1] > 2 or laser_data[lado_direito, 1] < laser_data[lado_esquerdo, 1]:
      v = 0
      w = np.deg2rad(30)
    
    # Isso é o modelo cinemático, estudaremos detalhadamente depois!
    wl = v / r - (w * L) / (2 * r)
    wr = v / r + (w * L) / (2 * r)

    # ---------------------------------------------------------------------\
    robot_grid_x, robot_grid_y = discretize_pos(robot_t[0], robot_t[1], configs)

    readings = []
    for (ang, dist) in laser_data:
      if dist < configs["sensor-max-dist"]:
        dist += np.random.normal(0, configs["noise-std"])
        ang += np.random.normal(0, configs["noise-std"])

        x = dist * np.cos(ang)
        y = dist * np.sin(ang)
        real_pos = floor_m @ np.array([x, y, 0, 1])

        grid_x, grid_y = discretize_pos(real_pos[0], real_pos[1], configs)
        rr, cc = line(robot_grid_x, robot_grid_y, grid_x, grid_y)

        line_pos = [(x, y) for x, y in zip(rr, cc)]
        readings.append((line_pos[1:], dist))

    update_map(readings)
    
    # Enviando velocidades
    sim.simxSetJointTargetVelocity(clientID, l_wheel, wl, sim.simx_opmode_streaming + 5)
    sim.simxSetJointTargetVelocity(clientID, r_wheel, wr, sim.simx_opmode_streaming + 5)        

    t = t + dt  
    lastTime = now

  # Parando o robô    
  sim.simxSetJointTargetVelocity(clientID, r_wheel, 0, sim.simx_opmode_oneshot_wait)
  sim.simxSetJointTargetVelocity(clientID, l_wheel, 0, sim.simx_opmode_oneshot_wait)        
    
  sim.simxStopSimulation(clientID, sim.simx_opmode_blocking)         
  sim.simxFinish(clientID)
else:
  print ("Failed connecting to remote API server")

plt.imshow(grid, cmap="Greys")
plt.show()
    
print ("Program ended")